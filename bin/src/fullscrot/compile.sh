#!/bin/bash
[[ -z $GOPATH ]] && exit 1

[[ ! -d "$GOPATH/src/github.com/kbinani/screenshot/" ]] && {
	go get -u github.com/kbinani/screenshot
}

[[ -f "fullscrot" ]] && { rm "fullscrot" || exit 1; }

CC=/usr/bin/musl-gcc go build --ldflags '-linkmode external -extldflags "-static"' -o "${Destination}" fullscrot.go
