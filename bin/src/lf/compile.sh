#!/bin/bash
[[ -z $GOPATH ]] && exit 1
Destination="$(pwd)/$(dirname "$0")/lf"

PWD="$GOPATH/src/github.com/gokcehan/lf/"
[[ ! -d "$PWD" ]] && {
	go get -u github.com/gokcehan/lf
}

[[ -f "$Destination" ]] && { rm "$Destination" || exit 1; }

cd "$PWD"
CC=/usr/bin/musl-gcc go build --ldflags '-linkmode external -extldflags "-static"' -o "${Destination}" 
