# Rainy

My first take at a simple, title-bar-less rice with a minimalistic panel.

## Warning

Warning! This dotfile is **very** hard-coded. You *will have to* edit some of the things to make it work for you. Also note that this is tailored towards Alpine.

## Screenshots

![Dirty2](https://gitlab.com/diamondburned/rainy/raw/master/Screenshots/dirty2.png)

![Fullscreen](https://gitlab.com/diamondburned/rainy/raw/master/Screenshots/fullscr.png)

![Dirty1](https://gitlab.com/diamondburned/rainy/raw/master/Screenshots/dirty1.png)

![Clean](https://gitlab.com/diamondburned/rainy/raw/master/Screenshots/clean.png)
