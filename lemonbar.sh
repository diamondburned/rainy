#!/bin/sh
trap exit SIGINT

width=$(xrandr | grep "  .*+" | cut -d' ' -f4 | cut -dx -f1)

# Requirements: siji + tewi
function update() {
	right="%{r}$(date +"%A, %B %d, %Y    %I:%M %p ")   "
	left="%{l}    ${HOSTNAME}"

	echo "$left$right"
}

if [[ "$1" != "" ]]; then
	dimensions="$1"
else
	dimensions="${width}x15+0+0"
fi

font="tewi"
[[ "$2" != "" ]] && font="$2"

while :; do
	update
	sleep 0.5
done | lemonbar -p -f '-*-'"$font"'-medium-r-normal-*-11-*-*-*-*-*-*-1' -f '-wuncon-siji-medium-r-normal--10-100-75-75-c-80-iso10646-1' -B "#01030D" -F "#A9B8D2" -g "${dimensions}"
