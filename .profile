TERM="xterm-256color"
PS1='\[\033[00;34m\]${HOSTNAME} \[\033[01;34m\]\W \[\033[0m\]'
PATH="/root/.assets/bin/:$PATH"
alias startx="startx -- -nocursor"
alias xinit="startx -- -nocursor"
